const merge = require('webpack-merge');
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const VueSSRServerPlugin = require('vue-server-renderer/server-plugin');
const config = require('./webpack.config.server.base');

const entries = [
  {
    entry: path.resolve(__dirname, 'src/app/entry-server-listing.js'),
    filename: 'vue-ssr-server-list-bundle.json'
  },
  {
    entry: path.resolve(__dirname, 'src/app/entry-server-product.js'),
    filename: 'vue-ssr-server-product-bundle.json'
  },
  {
    entry: path.resolve(__dirname, 'src/app/entry-server-tqbui.js'),
    filename: 'vue-ssr-server-tqbui-bundle.json'
  }
];

const configs = entries.map(entry =>
  merge(config, {
    mode: process.env.NODE_ENV,
    entry: entry.entry,
    plugins: [
      new VueSSRServerPlugin({ filename: entry.filename }),
      new VueLoaderPlugin()
    ],
    optimization: {
      namedModules: true,
      splitChunks: {
        name: 'vendor',
        minChunks: 4
      }
    }
  })
);

module.exports = configs;
