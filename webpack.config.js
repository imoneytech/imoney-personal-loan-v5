const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const SpritesmithPlugin = require("webpack-spritesmith");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const CompressionPlugin = require("compression-webpack-plugin");
const BrotliPlugin = require("brotli-webpack-plugin");

const config = require("./config");

const { assetsPlaceHolder } = config.app;
console.log(
 `/src/app/components/blocks/faq/${config.app.region}-${
  config.app.lang
 }-faq.vue`
);

const webpackConfig = {
 mode: process.env.NODE_ENV,
 context: __dirname,
 entry: {
  listing: ["./src/index-listing.js"],
  thankyou: "./src/index-thankyou.js",
  applyJs: "./src/apply.js",
  product: "./src/index-product.js",
  error: "./src/index-error.js",
  tqbui: "./src/index-tqbui.js",
  vendor: ["vue", "buefy"]
 },
 target: "web",
 output: {
  path: `${__dirname}/dist`,
  filename: `${assetsPlaceHolder}/js/[name]_[chunkhash].js`,
  publicPath: "/"
 },
 module: {
  rules: [
   {
    test: /\.modernizrrc.js$/,
    loader: "modernizr"
   },
   {
    test: /\.modernizrrc(\.json)?$/,
    use: ["modernizr-loader", "json-loader"]
   },
   {
    test: /\.(sa|sc|c)ss$/,
    use: [
     {
      loader: MiniCssExtractPlugin.loader,
      options: {
       hmr: process.env.NODE_ENV === "development"
      }
     },
     "css-loader",
     "postcss-loader",
     "sass-loader"
    ]
   },
   {
    test: /\.m?js$/,
    exclude: /(node_modules|bower_components)/,
    use: {
     loader: "babel-loader",
     options: {
      presets: ["@babel/preset-env"]
     }
    }
   },
   {
    test: /\.vue$/,
    loader: "vue-loader"
   },
   {
    test: /\.html$/,
    use: [
     {
      loader: "html-loader",
      options: {
       minimize: true
      }
     }
    ]
   },
   {
    test: /\.(woff|woff2|eot|ttf|otf)$/,
    use: [
     {
      loader: "file-loader",
      options: {
       name: `${assetsPlaceHolder}/font/[name].[ext]`
      }
     }
    ]
   },
   {
    test: /\.(png|jpg|gif|svg|pdf)$/,
    use: [
     {
      loader: "file-loader",
      options: {
       name: `${assetsPlaceHolder}/img/[name].[ext]`
      }
     }
    ]
   }
  ]
 },
 resolve: {
  extensions: ["*", ".js", ".vue", ".json"],
  alias: {
   vue$: "vue/dist/vue.esm.js",
   "faq.vue": path.join(
    __dirname,
    `/src/app/components/blocks/faq/${config.app.region}-${
     config.app.lang
    }-faq.vue`
   ),
   "faq-online-loan.vue": path.join(
    __dirname,
    `/src/app/components/blocks/faq/${config.app.region}-${
     config.app.lang
    }-faq-online-loan.vue`
   ),
   "faq-business-loan.vue": path.join(
    __dirname,
    `/src/app/components/blocks/faq/${config.app.region}-${
     config.app.lang
    }-faq-business-loan.vue`
   ),
   "formModal.vue": path.join(
    __dirname,
    `/src/app/components/modals/applyForms/${config.app.region}/formModal.vue`
   ),
   "applyForm.vue": path.join(
    __dirname,
    `/src/app/components/modals/applyForms/${config.app.region}/applyForm.vue`
   ),
   "_inc/_icon-base.scss": path.join(
    __dirname,
    "src/imoney-assets/scss/_inc/_icon-base.scss"
   ),
   "./icon-base.png": path.join(
    __dirname,
    "src/imoney-assets/img/sprite/icon-base.png"
   ),
   font: path.join(__dirname, "src/imoney-assets/font"),
   "./font": path.join(__dirname, "src/imoney-assets/font"),
   "./img": path.join(__dirname, "src/img"),
   "./locals.json": path.join(__dirname, `locales/${config.app.lang}.json`),
   modernizr$: path.resolve(__dirname, ".modernizrrc"),
   "~": path.resolve(__dirname, "node_modules"),
   "imoney-assets/scss": path.join(__dirname, "src/imoney-assets/scss/"),
   "./imoney-assets/img": path.join(__dirname, "src/imoney-assets/img/"),
   "bus.js": path.join(__dirname, "src/app/bus.js"),
   "helper.js": path.join(
    __dirname,
    `src/app/lib/${config.app.region}/helper.js`
   ),
   "pdfobject.js": path.join(__dirname, "src/app/lib/pdfobject.js"),
   calculation: path.join(
    __dirname,
    `/src/app/lib/${config.app.region}/calculation.js`
   ),
   filter: path.join(__dirname, `/src/app/lib/${config.app.region}/filter.js`),
   sideFilter: path.join(
    __dirname,
    `/src/app/components/${config.app.region}/sideFilter.vue`
   ),
   calculator: path.join(
    __dirname,
    `/src/app/components/${config.app.region}/calculator.vue`
   ),
   "filterMobile.vue": path.join(
    __dirname,
    `/src/app/components/modals/${config.app.region}/filterMobile.vue`
   ),
   store: path.join(__dirname, `/src/app/store/${config.app.region}/index.js`),
   numberFormat: path.join(
    __dirname,
    `/src/app/lib/${config.app.region}/numberFormat.js`
   ),
   "pageMenu.vue": path.join(
    __dirname,
    `/src/app/components/${config.app.region}/pageMenu.vue`
   ),
   "./product.vue": path.join(
    __dirname,
    `/src/app/pages/${config.app.region}/product.vue`
   ),
   "./listing.vue": path.join(
    __dirname,
    `/src/app/pages/${config.app.region}/listing.vue`
   ),
   "./filter.scss": path.join(
    __dirname,
    `/src/scss/${config.app.region}/filter.scss`
   ),
   "./list-item.scss": path.join(
    __dirname,
    `/src/scss/${config.app.region}/list-item.scss`
   )
  },
  modules: ["node_modules", "sprite"]
 },
 plugins: [
  new CleanWebpackPlugin("dist"),
  new VueLoaderPlugin(),
  new CompressionPlugin({
   filename: "[path].gz[query]",
   algorithm: "gzip",
   test: /\.js$|\.css$|\.html$/,
   threshold: 10240,
   minRatio: 0.8,
   deleteOriginalAssets: true,
   cache: true
  }),
  new BrotliPlugin({
   asset: "[path].br[query]",
   test: /\.js$|\.css$|\.html$/,
   threshold: 10240,
   minRatio: 0.8
  }),
  new webpack.optimize.OccurrenceOrderPlugin(),
  new SpritesmithPlugin({
   src: {
    cwd: path.resolve(__dirname, "src/imoney-assets/img/sprite/icon-base"),
    glob: "*.png" // TO-DO : combine with .svg files
   },
   target: {
    image: path.resolve(
     __dirname,
     "src/imoney-assets/img/sprite/icon-base.png"
    ), // generate img sprite to img folder to use in scss in this repo
    css: path.resolve(__dirname, "src/imoney-assets/scss/_inc/_icon-base.scss") // generate partial scss in this repo to use this img sprite
   },
   apiOptions: {
    cssImageRef: `/${assetsPlaceHolder}/img/icon-base.png`
   }
  }),
  new webpack.ProvidePlugin({
   $: "jquery",
   jQuery: "jquery",
   _: "underscore"
  }),
  new HtmlWebpackPlugin({
   filename: "listing.html",
   template: "src/listing.html",
   minify: {
    removeComments: true,
    collapseWhitespace: true,
    conservativeCollapse: true
   },
   chunks: ["applyJs", "listing", "vendor"]
  }),
  new HtmlWebpackPlugin({
   filename: "product.html",
   template: "src/product.html",
   minify: {
    removeComments: true,
    collapseWhitespace: true,
    conservativeCollapse: true
   },
   chunks: ["applyJs", "product", "vendor"]
  }),
  new HtmlWebpackPlugin({
   filename: "thankyou.html",
   template: "src/thankyou.html",
   minify: {
    collapseWhitespace: true
   },
   chunks: ["thankyou", "vendor"]
  }),
  new HtmlWebpackPlugin({
   filename: "tqbui.html",
   template: "src/tqbui.html",
   minify: {
    removeComments: true,
    collapseWhitespace: true,
    conservativeCollapse: true
   },
   chunks: ["tqbui", "vendor"]
  }),
  // new HtmlWebpackPlugin({
  //   filename: 'thankyou-bui.html',
  //   template: 'src/thankyou-bui.html',
  //   minify: {
  //     collapseWhitespace: true
  //   },
  //   chunks: ['thankyou', 'vendor']
  // }),
  new HtmlWebpackPlugin({
   filename: "templates/thankyou.html",
   template: `./src/imoney-assets/templates/${config.app.region}/${
    config.app.lang
   }/thankyou-personal-loan.html`, // default
   minify: {
    collapseWhitespace: true
   },
   inject: false
  }),
  new MiniCssExtractPlugin({
   // Options similar to the same options in webpackOptions.output
   // both options are optional
   filename: `${assetsPlaceHolder}/css/[name].min.css`
  }),
  new HtmlWebpackPlugin({
   filename: "error.html",
   template: "src/error.html",
   minify: {
    collapseWhitespace: true
   },
   chunks: ["error", "vendor"]
  }),
  new HtmlWebpackPlugin({
   filename: "templates/404.html",
   template: `./src/imoney-assets/templates/${config.app.region}/${
    config.app.lang
   }/404.html`, // default
   minify: {
    collapseWhitespace: true
   },
   inject: false
  }),
  new HtmlWebpackPlugin({
   filename: "templates/base.html",
   template: "./src/base.html",
   minify: {
    collapseWhitespace: true
   },
   inject: false
  }),
  // minify partial html files
  new HtmlWebpackPlugin({
   filename: "templates/topbar.html",
   template: `./src/imoney-assets/templates/${config.app.region}/${
    config.app.lang
   }/topbar.html`,
   minify: {
    collapseWhitespace: true
   },
   inject: false
  }),
  new HtmlWebpackPlugin({
   filename: "templates/navigation.html",
   template: `./src/imoney-assets/templates/${config.app.region}/${
    config.app.lang
   }/navigation.html`,
   minify: {
    collapseWhitespace: true
   },
   inject: false
  }),
  new HtmlWebpackPlugin({
   filename: "templates/mb-navigation.html",
   template: `./src/imoney-assets/templates/${config.app.region}/${
    config.app.lang
   }/mb-navigation.html`, // default
   minify: {
    collapseWhitespace: true
   },
   inject: false
  }),
  new HtmlWebpackPlugin({
   filename: "templates/footer.html",
   template: `./src/imoney-assets/templates/${config.app.region}/${
    config.app.lang
   }/footer.html`, // default
   minify: {
    collapseWhitespace: true
   },
   inject: false
  })
 ],
 optimization: {
  namedModules: true,
  splitChunks: {
   name: "vendor",
   minChunks: 4
  }
 }
};

module.exports = webpackConfig;
