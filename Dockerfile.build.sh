#!/usr/bin/env bash

if [ -z $1 ]; then
    echo "No appname provided"
    exit 1
fi

if [ -z $2 ]; then
    echo "No tag provided"
    exit 1
fi

if [ -z $3 ]; then
    echo "No region provided"
    exit 1
fi

if [ -z $4 ]; then
    echo "No environment provided"
    exit 1
fi

if [ -z $5 ]; then
    echo "No git hash provided"
    exit 1
fi

if [ -z $6 ]; then
    echo "No lang provided"
    exit 1
fi

if [ -z $7 ]; then
    echo "No FT ENV provided"
    exit 1
fi

if [ -z $8 ]; then
    echo "No MARKET provided"
    exit 1
fi

project=$1
tag=$2
build_region=$3
env=$4
git_hash=$5
lang=$6
ENV=$7
MARKET=$8

docker build --no-cache --build-arg ssh_private_key="$(cat ~/.ssh/id_rsa)" --build-arg build_region=${build_region} --build-arg env=${env} --build-arg lang=${lang} --build-arg ENV=${ENV} --build-arg MARKET=${MARKET} -t ${project}-${build_region}:$tag -t ${project}-${build_region}:${git_hash} -f Dockerfile .  || exit 1
#gcloud docker -- push gcr.io/imoney-frontend/${project}-${build_region}:${git_hash} || exit 1
