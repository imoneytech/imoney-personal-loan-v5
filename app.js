global.newrelic = require('newrelic');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const compression = require('compression');
var expressStaticGzip = require('express-static-gzip');
// var sqlinjection = require('sql-injection');

const ejs = require('ejs');
let LRU = require('lru-cache');
ejs.cache = new LRU(1000); // LRU cache with 100-item limit

const i18n = require('i18n');

const app = express();
const helmet = require('helmet');
app.use(helmet());
app.use(compression());
// app.use(sqlinjection);

const config = (global.config = require('./config'));
try {
  require('./custom_modules/iMoneyNewrelic')(app);
} catch (e) {
  throw e;
};

const metadata = (global.metadata = require('./custom_modules/data_models/metatag'));
const PersonalLoan = (global.PersonalLoan = require('./custom_modules/data_models/personal-loan'));

const logger = require('./custom_modules/logger');

logger.info(config);
i18n.configure({
  locales: [config.app.lang],
  defaultLocale: config.app.lang,
  objectNotation: true,
  directory: `${__dirname}/locales`
});
app.use(i18n.init);

app.set('views', path.join(__dirname, 'dist'));
app.set('view engine', 'html');
app.engine('html', ejs.renderFile);

const routes = require('./routes/index');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());

app.use( `/${config.app.assetsPlaceHolder}`,
 expressStaticGzip(path.join(__dirname, `dist/${config.app.assetsPlaceHolder}`), {
  enableBrotli: true
 }));

app.use((req, res, next) => {
  metadata.update();
  PersonalLoan.fetch();
  next();
  // var queryParamSize = Object.size(req.query);
  // console.log('length', queryParamSize);
});
app.use('/', routes);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res) => {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
      app: global.config.app,
      bodyClass: `imoney-${global.config.app.region}`,
      region: global.config.app.region,
      lang: global.config.app.lang
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  console.log(err)
  // logger.warn(err);
  res.status(err.status || 500);
  const metatagDataresponse = {};
  const base = {};
  res.render('../dist/error.html', {
    message: err.message,
    error: {},
    app: global.config.app,
    pageTitle: metatagDataresponse.pageTitle,
    bodyClass: `imoney-${global.config.app.region}`,
    region: global.config.app.region,
    lang: global.config.app.lang,
    metaDescription: metatagDataresponse.metaDescription,
    metaKeyword: metatagDataresponse.metaKeyword,
    metaData: metatagDataresponse,
    meta: metatagDataresponse,
    itemData: metatagDataresponse,
    ogImage: metatagDataresponse.ogImage,
    fbAppId: global.config.app.fbAppId,
    data: '',
    base,
    assetsPlaceHolder: global.config.app.assetsPlaceHolder
  });
});

module.exports = app;
