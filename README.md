# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* iMoney v5 base - to be forked out to create web application as base for verticals.
* v0.9 (I just made this up)

### How do I get set up? ###

* Fork this repository
* Set Owner to : imoneytech
* Set Name of the repository to follow this format : imoney-5-[vertical-title]
* Clone to your machine
* Open terminal and run "npm install"
* Then, run "cp config/app.json.sample config/app.json"
* "npm run build:MY" to build your application for Malaysia.
* "npm run start:MY" to start running and watching your application for Malaysia.
* To see other regions, replace MY with either of these [MS, ID, PH, SG].
* Open browser and go to [sample page](http://localhost:3000/sample)

### devDependencies ###

* Node v8
* Webpack
* Vuejs
* Buefy

### Node Version ###

Switch to the right node version by `nvm use`

### Who do I talk to? ###

* Emad, Elvie or Sara


