import 'buefy/dist/buefy.css';
import './imoney-assets/scss/redesign-global.scss';
import './imoney-assets/js/top-mainnav';

import './imoney-assets/img/icon-new.gif';
import './imoney-assets/img/preloader.gif';
import './imoney-assets/img/img_buildings.png';
import './imoney-assets/img/sprite/icon-base.png';

// MY
import './imoney-assets/img/my/dropdown-pl-alliance.png';
import './imoney-assets/img/my/dropdown-pl-alliance-ms.png';
import './imoney-assets/img/my/dropdown-cc-hsbc.jpg';
import './imoney-assets/img/my/dropdown-bb-search.jpg';
import './imoney-assets/img/my/dropdown-cc-hsbc-bm.png';
import './imoney-assets/img/my/dropdown-bb-search-bm.jpg';
import './imoney-assets/img/my/dropdown-pl-alliance.png';
import './imoney-assets/img/my/dropdown-pl-alliance-ms.png';

// Logo
import './imoney-assets/img/my/imoney-malaysia-logo.png';
import './imoney-assets/img/id/aturduit-logo.png';
import './imoney-assets/img/sg/imoney-singapore-logo.png';
import './imoney-assets/img/ph/imoney-philippines-logo.png';

// Favicon
import './imoney-assets/img/my/favicon-my.png';
import './imoney-assets/img/id/favicon-id.png';
import './imoney-assets/img/sg/favicon-sg.png';
import './imoney-assets/img/ph/favicon-ph.png';

// Markazi Text
import './imoney-assets/font/MarkaziText-Regular.ttf';
// OpenSans Light
import './imoney-assets/font/OpenSans-Light-webfont.eot';
import './imoney-assets/font/OpenSans-Light-webfont.ttf';
import './imoney-assets/font/OpenSans-Light-webfont.woff';
// OpenSans Regular
import './imoney-assets/font/OpenSans-Regular-webfont.eot';
import './imoney-assets/font/OpenSans-Regular-webfont.ttf';
import './imoney-assets/font/OpenSans-Regular-webfont.woff';
// OpenSans Semibold
import './imoney-assets/font/OpenSans-Semibold-webfont.eot';
import './imoney-assets/font/OpenSans-Semibold-webfont.ttf';
import './imoney-assets/font/OpenSans-Semibold-webfont.woff';
// OpenSans Bold
import './imoney-assets/font/OpenSans-Bold-webfont.eot';
import './imoney-assets/font/OpenSans-Bold-webfont.ttf';
import './imoney-assets/font/OpenSans-Bold-webfont.woff';
