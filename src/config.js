const merge = require('lodash/merge');
const { getEnv, getRawMarket } = require('./helpers/envHelper');
const { featureToggles } = require('./common/config/feature-toggles');

const serviceEnv = getEnv();

const config = {
 env: serviceEnv,
 market: getRawMarket()
};

const fileConfig = merge(featureToggles['common'], featureToggles[serviceEnv]);
const toggleConfig = { ...fileConfig, ...config };

exports.featureToggle = toggleConfig;
