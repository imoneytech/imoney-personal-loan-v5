import Vue from 'vue';
import Vuex from 'vuex';
import VueLazyload from 'vue-lazyload';

Vue.use(Vuex);
Vue.use(VueLazyload);

export const store = new Vuex.Store({
  state: {
    bankLoans: [],
    loans: [],
    loan: null,
    amount: 30000,
    tenure: 3,
    isAmountChanged: false,
    isTenureChanged: false,
    income: 5000,
    isFastApproval: false,
    isLoading: false,
    isPayday: false,
    isIslamic: false,
    isGovernmentServant: false,
    isLowestInterest: false,
    applyBtnFrom: "en_pl_listing",
    filterBanks: [],
    selectedProductForApply: {},
    defaultMode: true,
    urlRedirect: "",
    previewPDFActive: false,
    showPreview: false,
  },
  mutations: {
    init (state, loans) {
      state.loans = loans;
    },
    setDefaultMode (state, value) {
      state.defaultMode = value;
    },
    setAmount (state, value) {
      state.amount = value;
    },
    setTenure (state, value) {
      state.tenure = value;
    },
    setApplyBtnFrom (state, value) {
      state.applyBtnFrom = value;
    },
    setIsLoading(state, value) {
      state.isLoading = value;
    },
    setIncome (state, value) {
      state.income = value;
    },
    setIsAmountChanged (state, value) {
      state.isAmountChanged = value;
    },
    setIsTenureChanged (state, value) {
      state.isTenureChanged = value;
    },
    setFastApproval (state, value) {
      state.isFastApproval = value;
    },
    setIsPayday (state, value) {
      state.isPayday = value;
    },
    setIslamic (state, value) {
      state.isIslamic = value;
    },
    setGovernmentServant (state, value) {
      state.isGovernmentServant = value;
    },
    setLowestInterest (state, value) {
      state.isLowestInterest = value;
    },
    setPersonalBankLoan (state, value) {
      state.bankLoans = value;
    },
    setPersonalLoan (state, value) {
      state.loan = value;
    },
    setFilterBank (state, value) {
      state.filterBanks = value;
    },
    filterLoans (state, value) {
      Vue.set(state, 'loans', [...value]);
    },
    selectedProductForApply (state, value) {
      state.selectedProductForApply = value;
    },
    setURL (state, value) {
      state.urlRedirect = value;
    },
  },

  getters: {
   email: email => state.email,
   amount: amount => state.amount,
   income: income => state.income,
   applyBtnFrom: applyBtnFrom => state.applyBtnFrom
  },

  actions: {
    setAmount({ commit }, value) {
      commit("setAmount", value);
    },
    setIncome({ commit }, value) {
      commit("setIncome", value);
    },
    setApplyBtnFrom({ commit }, value) {
      commit("setApplyBtnFrom", value);
    },
    setPreviewPDFActive(state, value) {
      state.previewPDFActive = value;
    },
    setShowPreview(state, value) {
      state.showPreview = value;
    },
  }

});
