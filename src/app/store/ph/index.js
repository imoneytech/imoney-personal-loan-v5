import Vue from 'vue';
import Vuex from 'vuex';
import VueLazyload from 'vue-lazyload';

Vue.use(Vuex);
Vue.use(VueLazyload);

// console.log(window.__PRELOADED_STATE__,'teeeeeoooo')

export const store = new Vuex.Store({
  state: {
    bankLoans: [],
    loans: [],
    loan: null,
    amount: 200000,
    tenure: 1,
    income: 30000,
    isFastApproval: false,
    isLoading: false,
    isIslamic: false,
    isGovernmentServant: false,
    isLowestInterest: false,
    filterBanks: [],
    selectedProductForApply: {},
    defaultMode: true,
    urlRedirect: "",
    applyBtnFrom: "en_pl_listing",
    previewPDFActive: false,
    showPreview: false,
    // ----
    isFeaturesLongerRepayment:false,
    isFeaturesHighLoan:false,
    isFeaturesLoans:false,
    BTB: false,
    BTC: false
  },
  mutations: {
    init (state, loans) {
      state.loans = loans;
    },
    setDefaultMode (state, value) {
      state.defaultMode = value;
    },
    setAmount (state, value) {
      state.amount = value;
    },
    setTenure (state, value) {
      state.tenure =  value;
    },
    setApplyBtnFrom (state, value) {
      state.applyBtnFrom = value;
    },
    setIsLoading(state, value) {
      state.isLoading = value;
    },
    setIncome (state, value) {
      state.income = value;
    },
    setFastApproval (state, value) {
      state.isFastApproval = value;
    },
    setFeaturesHighLoan (state, value) {
      state.isFeaturesHighLoan = value;
    },
    setFeaturesLoans (state, value) {
      state.isFeaturesLoans = value;
    },
    setBTB (state, value) {
      state.BTB = value;
    },
    setBTC (state, value) {
      state.BTC = value;
    },
    setFeaturesLongerRepayment (state, value) {
      state.isFeaturesLongerRepayment = value;
    },
    setLowestInterest (state, value) {
      state.isLowestInterest = value;
    },
    setPersonalBankLoan (state, value) {
      state.bankLoans = value;
    },
    setPersonalLoan (state, value) {
      state.loan = value;
    },
    setFilterBank (state, value) {
      state.filterBanks = value;
    },
    filterLoans (state, value) {
      Vue.set(state, 'loans', [...value]);
    },
    selectedProductForApply (state, value) {
      state.selectedProductForApply = value;
    },
    setURL (state, value) {
      state.urlRedirect = value;
    },
  },

  getters: {
   email: email => state.email,
   amount: amount => state.amount,
   income: income => state.income,
   applyBtnFrom: applyBtnFrom => state.applyBtnFrom
  },
  actions: {
    setAmount({ commit }, value) {
      commit("setAmount", parseInt(value));
    },
    setIncome({ commit }, value) {
      commit("setIncome", parseInt(value));
    },
    setApplyBtnFrom({ commit }, value) {
      commit("setApplyBtnFrom", value);
    },
    setPreviewPDFActive(state, value) {
      state.previewPDFActive = value;
    },
    setShowPreview(state, value) {
      state.showPreview = value;
    },
  }

});
