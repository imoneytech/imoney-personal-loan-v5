import Vue from 'vue';
import Buefy from 'buefy';
import Vuelidate from 'vuelidate';
import "../scss/form-steps.scss";

import {
  store
} from 'store';
Vue.use(Vuelidate);

Vue.use(Buefy);

// export a factory function for creating fresh app, router and store
// instances
export default function createApp (App, context, translate) {
  Vue.filter('translate', (...args) => translate(...args));
  const app = new Vue({
    // the root instance simply renders the App component.
    store,
    render: h => h(App, {
      props: {
        context,
        translate
      }
    })
  });
  Vue.mixin({
    data: function () {
      return {
        config : context.config,
      }
    },
    methods: {
      translate (...args) {
        return translate(...args);
      } }
  });
  return {
    app
  };
};
