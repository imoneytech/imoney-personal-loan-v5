import App from './listing.vue';
import createApp from './app';

export default context => {
  const { app } = createApp(App, context, context.tr);
  return app;
};
