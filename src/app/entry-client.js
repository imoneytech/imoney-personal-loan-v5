import translate from './lib/translation';
import App from './listing.vue';
import createApp from './app';

// client-specific bootstrapping logic...
const { app } = createApp(App, window.__PRELOADED_STATE__, translate);

// this assumes App.vue template root element has `id="app"`
app.$mount('#app');
