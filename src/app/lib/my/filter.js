// Class for filtering the personal loan objects
export default class FilterPersonalLoan {
  constructor (personalLoans, fastApproval, isPayday, isIslamic, governmentServant, filterBanks) {
    this.personalLoans = personalLoans;
    this.fastApproval = fastApproval;
    this.isPayday = isPayday;
    this.isIslamic = isIslamic;
    this.governmentServant = governmentServant;
    this.filterBanks = filterBanks;
    this.ratesList = [];
    // this.lowestInterest = lowestInterest
    this.arr = [];
  }

  filterProducts () {
    let checkFilter = {
      fast_approval: this.fastApproval,
      is_payday: this.isPayday,
      government_only: this.governmentServant,
      is_islamic: this.isIslamic
    }

    let ifFilterAllFalse = Object.keys(checkFilter).every((k) => checkFilter[k] === false);
    this.arr = this.personalLoans.filter((value, index, array) => {
      let filterBank = this.filterBanks.indexOf(value.personalLoanBank.bank.name) >= 0;
      let result = true;
      if (ifFilterAllFalse && this.filterBanks.length > 0) {
        result = result && filterBank;
      } else {
        for (let key in checkFilter) {
        
          if (checkFilter[key]) {
            if (this.filterBanks.length > 0) {
              result =  result && value[key] && filterBank;
            } else {
              result =  result && value[key];
            }
          } 
        }
      }
      return result;
    });
  }

  /* 
    This function will take interest rates of the each personal loan 
    and check if it's null or not
  */
  lowestInterestRate () {
    this.personalLoans.forEach(personalLoan => {
      const {
        personal_loan_rates
      } = personalLoan;

      personal_loan_rates.forEach(rates => {
        if (rates.interest_rate !== null) {
          this.ratesList.push(rates);
        }
      });
    });
  }

  /* 
   Once we got all the interest rates in an array 
   then sort it based on accending order
  */
  returnLowestInterestRate () {
    this.lowestInterestRate();
    this.ratesList.sort((a, b) => a.interest_rate - b.interest_rate);
    return this.ratesList[0];
  }


  /* 
    The main function filter the products and return the array
  */
  main () {
    this.filterProducts();
    return this.arr;
  }
}
