/* eslint-disable eol-last */
/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */

/*
   - Calculator class
   - with 10 attributes
   - defaultMode, personalLoans, amount, tenure, income, monthlyRepayment,
   - totalAmountPayable, totalInterestPayable, promo, result
*/
export default class Calculator {
  constructor (defaultMode, personalLoans, amount, tenure, income, islamicPage) {
    this.defaultMode = defaultMode;
    this.personalLoans = personalLoans;
    this.amount = parseInt(amount);
    this.tenure = parseInt(tenure);
    this.income = income;
    this.monthlyRepayment = 0;
    this.totalAmountPayable = 0;
    this.totalInterestPayable = 0;
    this.promo = [];
    this.result = [];
  }

  // eslint-disable-next-line consistent-return
  /*
   - This function compare the (amount or tenure or income) that the user entered
   - with the personal loan products -> amount, tenure income
   - Check whether it is within the range
   - By using filterRatesBasedOnInterestFormat function to filter the rates
 */
  filterRates (personalLoan) {
    const arr = [];
    // eslint-disable-next-line prefer-destructuring

    // Get the interest rates from the personal loan product
    const {
      personal_loan_rates
    } = personalLoan;

    // Compare the value that the user entered and the personal loan product's value
    const checkAmount =
   this.amount < personalLoan.min_amount ||
   this.amount > personalLoan.max_amount;
    const checkTenure =
   this.tenure < personalLoan.min_tenure ||
   this.tenure > personalLoan.max_tenure;
    const checkIncome = this.income < personalLoan.min_income;
    const returnValidity = checkAmount || checkTenure || checkIncome;

    if (personalLoan.sticky_featured_type) {
      if (returnValidity) {
        arr.push(-1);
      } else {
        this.filterRatesBasedOnInterestFormat(personal_loan_rates, arr);
      }
    } else {
      if (!returnValidity) {
        this.filterRatesBasedOnInterestFormat(personal_loan_rates, arr);
      }
    }

    // In the array get the lowest interest rate since that is already matched
    // [1.3, 5.6] -> 1.3
    if (arr.length > 0) {
      const max = arr.reduce((acc, i) => (i < acc ? i : acc));
      return max;
    }
  }

  /*
   - Compare the value that the user entered eg: amount, tenure, income
   - with the personal loan rates value
   - and if it is within the range then return the interest Rate(s)
 */
  filterRatesBasedOnInterestFormat (personal_loan_rates, arr) {
    for (let i = 0; i < personal_loan_rates.length; i += 1) {
      let interestFormat = personal_loan_rates[i].interest_format;
      let productTenure =
    parseInt(this.tenure) === personal_loan_rates[i].tenure_type;
      let productTenureRange =
    parseInt(this.tenure) >= personal_loan_rates[i].tenure_type;
      let productAmount = this.amount >= personal_loan_rates[i].amount;
      let productMinSalary = this.income >= personal_loan_rates[i].min_salary;
      let interestRate = personal_loan_rates[i].interest_rate;

      if (interestFormat === 'tenure' && productTenure) {
        arr.push(interestRate);
      } else if (interestFormat === 'amount' && productAmount) {
        arr.push(interestRate);
      } else if (interestFormat === 'income' && productMinSalary) {
        arr.push(interestRate);
      } else if (
        interestFormat === 'amount_tenure' &&
    (productAmount && productTenure)
      ) {
        arr.push(interestRate);
      } else if (interestFormat === 'tenurerange' && productTenureRange) {
        arr.push(interestRate);
      } else if (
        interestFormat === 'tenure_income' &&
    (productTenure && productMinSalary)
      ) {
        arr.push(interestRate);
      }
    }
  }

  /*
   - This function calculate the monthly repayment
   - the Formula:
   - ((1 + ((interestRate / 100) * tenure)) * amount) / (tenure * 12);
 */
  calcMonthlyRepayment (interestRate) {
    this.monthlyRepayment =
   ((1 + (interestRate / 100) * this.tenure) * this.amount) /
   (this.tenure * 12);
    return this.monthlyRepayment;
  }

  /*
   - This function calculate the total amount payable
   - the Formula:
   - monthlyRepayment * (tenure * 12)
 */
  calcTotalAmountPayable () {
    this.totalAmountPayable = this.monthlyRepayment * (this.tenure * 12);
    return this.totalAmountPayable;
  }

  /*
   - This function calculate the total interest payable
   - the Formula:
   - totalAmountPayable - amount
 */
  calcTotalInterestPayable () {
    this.totalInterestPayable = this.totalAmountPayable - this.amount;
    return this.totalInterestPayable;
  }

  /*
 - Check if the interest rate is not within the range (if it is "-") then
 - set all the calculations
 - monthlyRepayment, totalAmountPayable,totalInterestPayable -> "-"
 - else calculate it and return to the user
*/
  calculate (personalLoan) {
    if (personalLoan.productInterestRate === '-') {
      personalLoan.monthlyRepayment = '-';
      personalLoan.totalAmountPayable = '-';
      personalLoan.totalInterestPayable = '-';
    } else {
      personalLoan.monthlyRepayment = this.calcMonthlyRepayment(
        personalLoan.productInterestRate
      ).toFixed(0);
      if (isNaN(personalLoan.monthlyRepayment)) {
        personalLoan.monthlyRepayment = '-';
        personalLoan.totalAmountPayable = '-';
        personalLoan.totalInterestPayable = '-';
      } else {
        personalLoan.totalAmountPayable = this.calcTotalAmountPayable().toFixed(0);
        personalLoan.totalInterestPayable = this.calcTotalInterestPayable().toFixed(
          0
        );
      }
    }
  }

  /*
  - Loop the personal loans and use
  - filterRates, to get the rates according to the user's input
 */
  filterRatesAllProducts () {
    this.personalLoans.forEach(personalLoan => {
      // eslint-disable-next-line no-undef
      // eslint-disable-next-line no-param-reassign)
      const filteredInterestRates = this.filterRates(personalLoan);
      if (typeof filteredInterestRates !== 'undefined') {
        if (filteredInterestRates === -1) {
          personalLoan.productInterestRate = '-';
          this.calculate(personalLoan);
          this.result.push(personalLoan);
        } else {
          personalLoan.productInterestRate = filteredInterestRates.toFixed(2);
          this.calculate(personalLoan);
          this.result.push(personalLoan);
        }
      }
    });
  }

  /*
    - Get a single product and filter the rates based
    - on the user's input amount, income, tenure
 */
  filterRatesProduct (personalLoan) {
    const filteredInterestRates = this.filterRates(personalLoan);
    if (typeof filteredInterestRates !== 'undefined') {
      if (filteredInterestRates === -1) {
        personalLoan.productInterestRate = '-';
        this.calculate(personalLoan);
        return personalLoan;
      } else {
        personalLoan.productInterestRate = filteredInterestRates.toFixed(2);
        this.calculate(personalLoan);
        return personalLoan;
      }
    }
  }


  /*
  - If default mode then sort based on loan score
  - If not default then dont sort based on loan score
 */
  finalSort () {
    this.filterRatesAllProducts();
  
    if (this.result.length > 0 && this.defaultMode) {
      // Filter promo based on loan score
      const filterLoanScore = this.result.filter(key => key.loan_score > 0);
      filterLoanScore.sort(
        (a, b) => a.productInterestRate - b.productInterestRate
      );

      // Filter promo based on loan score
      const filterNonLoanScore = this.result.filter(key => key.loan_score <= 0);
      filterNonLoanScore.sort(
        (a, b) => a.productInterestRate - b.productInterestRate
      );

      this.result = filterLoanScore.concat(filterNonLoanScore);

      const filterPromo = this.result.filter(key => key.promo === true);
      filterPromo.sort((a, b) => b.loan_score - a.loan_score);

      const filterNotPromo = this.result.filter(key => key.promo === false);
      filterNotPromo.sort((a, b) => b.loan_score - a.loan_score);
      this.result = filterPromo.concat(filterNotPromo);

      // this.checkIslamicPage() 
    } else {
      let nonStickyProduct = this.result.filter(
        key => key.productInterestRate !== '-'
      );
      this.result = nonStickyProduct.sort(
        (a, b) => a.productInterestRate - b.productInterestRate
      );
    }

    this.result = this.result.filter(item => item.monthlyRepayment !== '-');
    //  this.checkIslamicPage() 
  }

  /*
  - Function to run when calling calculator class in vue files
 */
  main () {
    this.finalSort();
    return this.result;
  }
}
