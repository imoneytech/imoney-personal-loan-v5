import json from './locals.json';

import { vsprintf } from 'sprintf-js';

let targetString;

function getObject (theObject) {
  let result = null;
  for (const prop in theObject) {
    if (prop === targetString) return theObject[prop];
    if (theObject[prop] instanceof Object) result = getObject(theObject[prop]);
    if (result) break;
  }
  return result;
}

/**
 * Right now this custom function only solves 2 cases: pure translation and
 * variable translation.
 * @param str
 * @param args
 * @returns {*}
 */
export default function translate (str, ...args) {
  const strToarray = str.split('.');
  targetString = strToarray[strToarray.length - 1];

  if (strToarray.length > 1) {
    const strIndex = json[strToarray[0]];
    return getObject(strIndex);
  }
  // console.log(str);
  return args.length ? vsprintf(json[str], args) : json[str] || str;
}
