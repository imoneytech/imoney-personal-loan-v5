
/*
  Function to add comma to a number that is
  more than 999 - Example 1000 -> 1,000
*/
export function addCommas (nStr) {
  let numString = nStr;
  numString += '';
  const x = numString.split('.');
  let x1 = x[0];
  const x2 = x.length > 1 ? `.${x[1]}` : '';
  const rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1,$2');
  }
  return x1 + x2;

}

export function addCommasnewph (nStr) {
  let numString = Math.round(nStr);
  numString += '';
  const x = numString.split('.');
  let x1 = x[0];
  const x2 = x.length > 1 ? `.${x[1]}` : '';
  const rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1,$2');
  }
  return x1 + x2;
}
/*
  - Find the minimum amount of loan for all
  - the personal loan products
  - Expected result = 1000
*/
function findMinimumAmount (bankPersonalLoan) {
  if (bankPersonalLoan.length > 0) {
    const min = bankPersonalLoan.reduce((acc, i) =>
      i.min_amount < acc.min_amount ? i : acc
    );
    return min.min_amount;
  }
}

/*
  - Find the maxium amount of loan for all
  - the personal loan products
  - Expected result = 3000000
*/
function findMaximumAmount (bankPersonalLoan) {
  if (bankPersonalLoan.length > 0) {
    const max = bankPersonalLoan.reduce((acc, i) =>
      i.max_amount > acc.max_amount ? i : acc
    );
    return max.max_amount;
  }
}

/*
  - return a specific message
  - if the amount that user entered is more or less
  - than the personal loan minimum or maximum
*/
function maxOrMinMessage (amount, minAmount, maxAmount,currency) {
  if (amount < minAmount) {
    return {
      checked: true,
      message: `Minimum loan amount is ₱`,
      amount: addCommas(minAmount)
    };
  } else if (amount > maxAmount) {
    return {
      checked: true,
      message: `Maximum loan amount is ₱`,
      amount: addCommas(maxAmount)
    };
  }

  return {
    checked: false
  };
}

/*
  - return a specific message
  - if the income that user entered is more or less
  - than the personal loan income or income
*/
function minIncomeMessage (income, loan,currency) {
  if (income < loan.min_income) {
    return {
      checked: true,
      message: 'Minimum Income is ₱ ',
      income: addCommas(loan.min_income)
    };
  }
  return {
    checked: false
  };
}

/*
  - This function is specifically for getting the min and max amount of all
  - personal loan products and set it to variables
  - and pass the result to the maxOrMinMessage handler
*/
export function checkMaxOrMinAmountAllProducts (personalLoanProducts, enteredAmount) {
  // Get the min or max for the personal loan products
  let minAmount = findMinimumAmount(personalLoanProducts);
  let maxAmount = findMaximumAmount(personalLoanProducts);

  return maxOrMinMessage(enteredAmount, minAmount, maxAmount);
}

/*
  - This function is specifically for getting the min amount of all
  - personal loan products and set it to a variable
  - and pass the result to the minIncomeMessage handler
*/
export function checkMinIncomeAllProducts (personalLoanProducts, income) {
  // Create Reduce for MinIncome
  const minLoan = personalLoanProducts.reduce((acc, i) =>
    i.min_income < acc.min_income ? i : acc
  );

  return minIncomeMessage(income, minLoan);
}

/*
  - This function is specifically for getting the min and max amount of
  - a personal loan product and set it to variables
  - and pass the result to the maxOrMinMessage handler
*/
export function checkMaxOrMinAmountProduct (loanProduct, enteredAmount,currency) {
  let minAmount = loanProduct.min_amount;
  let maxAmount = loanProduct.max_amount;

  return maxOrMinMessage(enteredAmount, minAmount, maxAmount,currency);
}

/*
  - This function is specifically for getting the min income of
  - a personal loan product and pass
  - the result to the maxOrMinMessage handler
*/
export function checkMinIncomeProduct (loan, income) {
  return minIncomeMessage(income, loan);
}

export function iRates (irates,loan) {
  // TODO: sort
  if(irates){
    irates.sort((a, b) => a.interest_rate - b.interest_rate);
  let firstInterestRate = (irates[0].interest_rate).toFixed(2);
  let lastInterestRate = (irates[irates.length - 1].interest_rate).toFixed(2);
  if (irates.length > 1 && firstInterestRate !== lastInterestRate) {
    return `${firstInterestRate}% ~ ${lastInterestRate}%`;
  } else if (firstInterestRate === lastInterestRate) {
    return `${firstInterestRate}%`;
  } else {
    return `${firstInterestRate}%`;
  } 
  }
  return loan.interest_rate_text
  
}
