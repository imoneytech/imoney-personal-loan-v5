export function submitToJapp(values, Loan) {
 // console.log('submitToJapp ', Loan);
 return new Promise(function(resolve, reject) {
  CampaignFactory("ph", undefined, function(factory) {
   let cam;
   let machineName = Loan.machine_name != null ? Loan.machine_name : undefined;
   if (machineName) {
    cam = factory.getByMachineName(Loan.machine_name);
   } else {
    cam = factory.getByNid(Loan.nid);
   }

   for (let key in values) {
    cam.setFieldValue(key, values[key]);
   }
   IMDevPropBag.add("prequalify", true);
   cam.submit(function(result, status, params) {
    if (status === "complete") {
     resolve(result);
    }
   });
  });
 });
}

export function creatArrayRange(start, end, loan) {
 let array = [];
 if (loan) {
  if (["online-loan","business-loan"].indexOf(loan.loan_type) >= 0) {
   for (var i = start; i <= end; i++) {
    array.push(i);
   }
   return array;
  } else {
    //   console.log('else .. ');

   for (var i = start; i <= end; i++) {
    array.push(i);
    if (i + 0.5 < end) {
     array.push(i + 0.5);
    }
   }
   return array;
  }
 }
}

export function creatArrayRangePh(start, end) {
 let array = [];

 for (var i = start; i <= end; i++) {
  array.push(i);
 }
//  console.log('creatArrayRangePh', array)
 return array;
}

export function sortObjAscending(o) {
 var sorted = {};
 var key;
 var a = [];

 for (key in o) {
  if (o.hasOwnProperty(key)) {
   a.push(key);
  }
 }

 a.sort();

 for (key = 0; key < a.length; key++) {
  sorted[a[key]] = o[a[key]];
 }
 return sorted;
}

export function getProductData() {
 let productData = {
  title: ""
 };
 if (Loan !== "undefined") {
  productData.title = Loan.title;
 }

 return productData;
}

export function checkIfProductIsIslamic(islamic) {
 let islamicString;
 if (islamic) {
  islamicString = "financing";
 } else {
  islamicString = "loan";
 }

 return islamicString.charAt(0).toUpperCase() + islamicString.slice(1);
}

export function capitalText(value) {
 return value[0].toUppercase() + value[1];
}

export function toTitleCase(str) {
 return str.replace(/\w\S*/g, function(txt) {
  return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
 });
}
