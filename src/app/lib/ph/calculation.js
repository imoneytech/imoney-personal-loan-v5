/* eslint-disable eol-last */
/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */

import { isRegExp } from "underscore";

/*
   - Calculator class
   - with 10 attributes
   - defaultMode, personalLoans, amount, tenure, income, monthlyRepayment,
   - totalAmountPayable, totalInterestPayable, promo, result
*/
export default class Calculator {
 constructor(defaultMode, personalLoans, amount, tenure, income, monthly) {
  this.defaultMode = defaultMode;
  this.personalLoans = personalLoans;
  this.amount = parseInt(amount);
  this.tenure = parseFloat(tenure);
  this.income = income;
  this.monthlyRepayment = 0;
  this.totalAmountPayable = 0;
  this.totalInterestPayable = 0;
  this.promo = [];
  this.result = [];
  this.monthly = monthly;
 }

 filterRates(personalLoan) {
  personalLoan.productInterestRate = personalLoan.interest_rate || 0;
 }

 filterRatesProduct(personalLoan) {
  this.filterRates(personalLoan);
  // personalLoan.productInterestRate = filteredInterestRates;
  this.calculate(personalLoan);
  return personalLoan;
 }

 /*
   - This function calculate the monthly repayment
   - the Formula:
   - ((1 + ((interestRate / 100) * tenure)) * amount) / (tenure * 12);
 */
 calcMonthlyRepayment(personalLoan) {
  let interestRate = personalLoan.interest_rate | 0;
  if (["online-loan","business-loan"].indexOf(personalLoan.loan_type) >= 0) {
   this.monthlyRepayment = personalLoan.totalAmountPayable / this.tenure;
   return this.monthlyRepayment | 0;
  } else {
   this.monthlyRepayment = personalLoan.totalAmountPayable / (this.tenure * 12);
   return this.monthlyRepayment | 0;
  }
 }
 /*
   - This function calculate the total amount payable
   - the Formula:
   - monthlyRepayment * (tenure * 12)
 */
 calcTotalAmountPayable(personalLoan) {
  let interestRate = personalLoan.interest_rate;
  if (["online-loan","business-loan"].indexOf(personalLoan.loan_type) >= 0) {
   this.totalAmountPayable =
    this.amount + this.amount * (interestRate / 100) * this.tenure;
   // ((1 + (interestRate / 100) * this.tenure) * this.amount) /
   // (this.tenure * 12);
   return this.totalAmountPayable;
  } else {
   this.totalAmountPayable =
    this.amount + this.amount * (interestRate / 100) * (this.tenure * 12);
   return this.totalAmountPayable;
  }
 }

 /*
   - This function calculate the total interest payable
   - the Formula:
   - totalAmountPayable - amount
 */
 calcTotalInterestPayable(personalLoan) {
  if (personalLoan.is_payday) {
   this.totalInterestPayable = personalLoan.totalAmountPayable - this.amount;
   return this.totalInterestPayable;
  } else {
   this.totalInterestPayable = this.totalAmountPayable - this.amount;
   return this.totalInterestPayable;
  }
 }

 /*
 - Check if the interest rate is not within the range (if it is "-") then
 - set all the calculations
 - monthlyRepayment, totalAmountPayable,totalInterestPayable -> "-"
 - else calculate it and return to the user
*/
 calculate(personalLoan) {
  if (personalLoan.is_payday) {
   personalLoan.totalAmountPayable = this.calcTotalAmountPayable(
    personalLoan
   ).toFixed(2);
   personalLoan.monthlyRepayment = this.calcMonthlyRepayment(
    personalLoan
   ).toFixed(2);
   personalLoan.totalInterestPayable = this.calcTotalInterestPayable(
    personalLoan
   ).toFixed(2);
  } else {
   personalLoan.totalAmountPayable = this.calcTotalAmountPayable(
    personalLoan
   ).toFixed(2);
   personalLoan.monthlyRepayment = this.calcMonthlyRepayment(
    personalLoan
   ).toFixed(2);
   personalLoan.totalInterestPayable = this.calcTotalInterestPayable(
    personalLoan
   ).toFixed(2);
  }
 }

 between(x, min, max) {
  return x >= min && x <= max;
 }

 /*
  - Loop the personal loans and use
  - filterRates, to get the rates according to the user's input
 */
 filterRatesAllProducts() {
  this.result = [];
  this.amount = parseInt(this.amount);
  this.personalLoans.forEach(personalLoan => {
   

   let pass = true;
   if (!personalLoan.promo) {
    if (
     !this.between(
      parseInt(this.amount),
      personalLoan.min_amount,
      personalLoan.max_amount
     )
    ) {
     pass = false;
    } else if (
     !this.between(
      parseFloat(this.tenure),
      personalLoan.min_tenure,
      personalLoan.max_tenure
     )
    ) {
     pass = false;
    } else if (parseInt(this.income) < personalLoan.min_income
    ) {
     pass = false;
    }
   }

   if (pass) {
    this.filterRates(personalLoan);
    this.calculate(personalLoan);
    this.result.push(personalLoan);
   }
  });
 }

 /*
  - If default mode then sort based on loan score
  - If not default then dont sort based on loan score
 */
 finalSort() {
  this.filterRatesAllProducts();

  // Filter promo based on loan score
  // const filterLoanScore = this.result.filter(key => key.loan_score > 0);
  // filterLoanScore.sort((a, b) => a.productInterestRate - b.productInterestRate);

  // // Filter promo based on loan score
  // const filterNonLoanScore = this.result.filter(key => key.loan_score <= 0);
  // filterNonLoanScore.sort(
  //  (a, b) => a.productInterestRate - b.productInterestRate
  // );

  // this.result = filterLoanScore.concat(filterNonLoanScore);

  // const filterPromo = this.result.filter(key => key.promo === true);
  // filterPromo.sort((a, b) => b.loan_score - a.loan_score);

  // const filterNotPromo = this.result.filter(key => key.promo === false);
  // filterNotPromo.sort((a, b) => b.loan_score - a.loan_score);
  // this.result = filterPromo.concat(filterNotPromo);

  // this.checkIslamicPage()

  //  this.checkIslamicPage()
 }

 /*
  - Function to run when calling calculator class in vue files
 */
 main() {
  this.finalSort();
  return this.result;
 }
}
