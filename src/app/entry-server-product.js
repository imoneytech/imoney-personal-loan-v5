import App from './product.vue';
import createApp from './app';

export default context => {
  const { app } = createApp(App, context, context.tr);
  return app;
};
