// Javascripts
import './assets';
import './js/global.js';

// Vue app
// import "./app/entry-client-provider";

// SCSS
// import "./scss/provider.scss";

// Images
import './img/product-page-sprites.png';
import './img/icon-star.svg';

