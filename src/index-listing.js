import Modernizr from 'modernizr';

// Javascripts
import './assets';
import './app/entry-client';
import './js/smooth-scroll';
import './js/sticky-top-bar';

// // Vue app

// // SCSS
// import './scss/page-listing.scss';
// import './scss/form-steps.scss';
// Above is moved to listing.vue it is clashing with the z-index of objects in product page

// Images
import "./img/eligibility-tool/01.svg";
import "./img/eligibility-tool/02.svg";
import "./img/eligibility-tool/03.svg";
import "./img/eligibility-tool/04.svg";
import "./img/eligibility-tool/05.svg";
import "./img/eligibility-tool/06.svg";
import './img/alliance_mock.pdf';
import './img/icon-star.svg';
