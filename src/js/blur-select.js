$(document).ready(() => {
  // Unfocus select dropdowns on main navigation hover
  $('.nav__list').hover(function () {
    $('select').blur();
  });
});
