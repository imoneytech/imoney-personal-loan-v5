$(document).ready(() => {
  // Sticky top bar
  // function to calculate sticky area position
  function getStartPosition (className) {
    var featurePosition = $(className).offset().top;
    return featurePosition;
  }
  function getEndPosition (className) {
    var featurePosition = $(className).offset().top;
    return featurePosition + $(className).outerHeight();
  }

  // get the position at start
  var startPosition = getStartPosition('.sticky-top-bar-area');
  var endPosition = getEndPosition('.sticky-top-bar-area');

  // recalculate the position on window resize
  $(window).resize(function () {
    startPosition = getStartPosition('.sticky-top-bar-area');
    endPosition = getEndPosition('.sticky-top-bar-area');
  });

  // activate the sticky header on body scroll
  $(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= startPosition && scroll <= endPosition) {
      $('#sticky-top-bar, #section-bar--desktop').addClass('slidein');
    } else {
      $('#sticky-top-bar, #section-bar--desktop').removeClass('slidein');
    }
  });

  // Desktop top bar
  // highlight current section when scrolling
  var section = document.querySelectorAll('.nav-scroll');
  var sections = {};
  var i = 0;

  Array.prototype.forEach.call(section, function (e) {
    sections[e.id] = e.offsetTop - 70;
  });

  window.onscroll = function () {
    var scrollPosition = document.documentElement.scrollTop || document.body.scrollTop;

    for (i in sections) {
      if (sections[i] <= scrollPosition) {
        $('.menu-center a').removeClass('active');
        		$('.menu-center a[href*=' + i + ']').addClass('active');
      }
    }
  };

  // Mobile bottom bar
  // scroll to section on selection
  var dropDownValue = $('#dropDown');
  dropDownValue.change(function () {
    var selectedSection = $(this.value);
    if (this.selectedIndex !== 0) {
      $('html, body').animate({
        scrollTop: selectedSection.offset().top - 15
			  }, 600
      );
    }
  });
});
