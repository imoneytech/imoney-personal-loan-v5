import Modernizr from 'modernizr';

// Javascripts
import './assets';
import './app/entry-client-product';
import './js/smooth-scroll';
import './js/sticky-top-bar';
import './js/blur-select';

// Vue app

// SCSS
// import "./scss/page-product.scss";
// import "./scss/form-steps.scss";
// Above is moved to product.vue because it is clashing with the z-index of objects in product page

// Images
import './img/img-books-pl.png';
import './img/icon-star.svg';
