// Javascripts
import './assets';
import './imoney-assets/js/thankyou';
import './imoney-assets/js/rss';

// Vue app

// SCSS
// import './imoney-assets/scss/thankyou.scss';
import './scss/page-thank-you-bui.scss'

// Images
import './imoney-assets/img/envelope-back.svg';
import './imoney-assets/img/envelope-close.svg';
import './imoney-assets/img/envelope-front.svg';
import './imoney-assets/img/envelope-open.svg';
import './imoney-assets/img/letter.svg';
import './imoney-assets/img/tick-success.svg';
import './imoney-assets/img/bg_user_review-sm.png';
import './imoney-assets/img/bg_user_review-mb.png';
import './img/sprite_bank-ui.png';