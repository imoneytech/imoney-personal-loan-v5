const merge = require('lodash/merge');
const { getRawMarket } = require('../../helpers/envHelper');

const market = getRawMarket();

const marketSpecific = require(`./${market}/feature-toggles.json`)

const commonConfig = {
 common: {
  featureToggleList: {}
 },
 local: {
  featureToggleList: {},
 },
 development: {
  featureToggleList: {},
 },
 staging: {
  featureToggleList: {},
 },
 production: {
  featureToggleList: {},
 },
};

exports.featureToggles =  merge(commonConfig, marketSpecific);