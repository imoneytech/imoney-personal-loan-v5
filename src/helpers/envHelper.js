const getMarket = () => {
    if (process && process.env && process.env.MARKET) {
        switch (process.env.MARKET) {
            case "imoneymy":
                return "MY";
            case "imoneyph":
                return "PH";
            case "aturduit":
                return "ID";
            default:
                return "MY";
        }
    }

    return "MY";
};

const getRawMarket = function () {
    if (process && process.env && process.env.MARKET) {
        return process.env.MARKET.toLowerCase();
    }

    return "imoneymy";
}

const getEnv = () => {
    if(process && process.env && process.env.ENV){
      return process.env.ENV;
    }

    return "";
  }

  exports.getEnv = getEnv;
  exports.getMarket = getMarket;
  exports.getRawMarket = getRawMarket;
