var JAPPConfig = function () {
  var jconfig = [
    {
      regionCode: 'my'
      , env: 'dev'
      , lcUrl: 'http://dev.formapp.ly/api/save/submission'
      , schemaUrl: 'http://dev.formapp.ly/api/campaigns'
    }
    , {
      regionCode: 'my'
      , env: 'prod'
      , lcUrl: 'https://apply.imoney.my/api/save/submission'
      , schemaUrl: 'https://apply.imoney.my/api/campaigns'
    }
    , {
      regionCode: 'ph'
      , env: 'dev'
      , lcUrl: 'http://ph-dev.formapp.ly/api/save/submission'
      , schemaUrl: 'http://ph-dev.formapp.ly/api/campaigns'
    }
    , {
      regionCode: 'ph'
      , env: 'prod'
      , lcUrl: 'https://apply.imoney.ph/api/save/submission'
      , schemaUrl: 'https://apply.imoney.ph/api/campaigns'
    }
    , {
      regionCode: 'id'
      , env: 'dev'
      , lcUrl: 'http://id-dev.formapp.ly/api/save/submission'
      , schemaUrl: 'http://id-dev.formapp.ly/api/campaigns'
    }
    , {
      regionCode: 'id'
      , env: 'prod'
      , lcUrl: 'https://apply.aturduit.com/api/save/submission'
      , schemaUrl: 'https://apply.aturduit.com/api/campaigns'
    }
    , {
      regionCode: 'sg'
      , env: 'dev'
      , lcUrl: 'http://sg-dev.formapp.ly/api/save/submission'
      , schemaUrl: 'http://sg-dev.formapp.ly/api/campaigns'
    }
    , {
      regionCode: 'sg'
      , env: 'prod'
      , lcUrl: 'https://apply.imoney.sg/api/save/submission'
      , schemaUrl: 'https://apply.imoney.sg/api/campaigns'
    }
  ];

  var prodUrls = [
    'offers.imoney.my'
    , 'offers.imoney.ph'
    , 'offers.imoney.sg'
    , 'offers.aturduit.com'
    , 'promosi.aturduit.com'
    , 'www.imoney.my'
    , 'www.imoney.ph'
    , 'www.imoney.sg'
    , 'www.aturduit.com'
  ];

  function getDefaultEnv() {
    if (prodUrls.indexOf(document.location.hostname) !== -1)
      return 'prod';

    if(document.location.hostname.indexOf('direct.') === 0)
      return 'dev';

    if ('https:' == document.location.protocol)
      return 'prod';

    return 'dev';
  }

  function get(regionCode, env) {
    if (typeof regionCode == 'undefined') {
      throw new Error('Region code is required!');
    }

    env = env || getDefaultEnv();

    return _.findWhere(jconfig, {
      regionCode: regionCode
      , env: env
    });
  }

  return {
    get: get
  };
}();
/*****************************
 /* End of JAPP Configuration
 /*****************************/


/*****************************
 /* Start of Property bags
 /*****************************/
var IMDevPropBag = function () {
  var props = {};

  return {
    getAll: function () {
      return props;
    }
    , add: function (propName, propValue) {
      props[propName] = propValue;
      return this;
    }
    , remove: function (propName) {
      if (typeof props[propName] === 'undefined') {
        throw new Error('Property "' + propName + '" is not available in this PropBag!');
      }

      props = _.omit(props, propName);
      return this;
    }
    , get: function (propName) {
      return getAll()[propName];
    }
  }
}();

var IMIMUPropBag = function () {
  return {
    getAll: getAll
    , get: get
  };

  function getAll() {
    var props = {};
    if (typeof userManager === 'undefined') return props;

    if (typeof userManager.utms !== 'undefined') {
      props = _.extend(props, {
        'UTSOURCE': userManager.utms.utmcsr  //CS: Campaign source
        , 'UTMEDIUM': userManager.utms.utmcmd  //MD: Medium
        , 'UTTERM': userManager.utms.utmctr  //TR: Campaign term
        , 'UTCAMPAIGN': userManager.utms.utmccn  //CN: Campaign name
      });
    }

    if (typeof userManager.data !== 'undefined') {
      props.imuTrackId = userManager.data.trackId;
      props.imuUserId = userManager.data.uId;
    }

    try {
      props.ga_client_id = ga.getAll()[0].get('clientId');
    }
    catch (e) {
      console.info('IMU: Could not extract clientId from ga!', e);
    }

    return props;
  }

  function get(propName) {
    return getAll()[propName];
  }
}();

var IMiMoneyCookiesPropBag = function () {
  return {
    getAll: getAll
    , get: get
  };

  function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split('=');
      if (decodeURIComponent(pair[0]) == variable) {
        return decodeURIComponent(pair[1]);
      }
    }
  }

  function getAll() {
    //Get properties from imoney.cookies.min.js if available
    var cookie_utm_src = typeof utmcsr != "undefined" ? utmcsr : '';
    var cookie_utm_medium = typeof utmcmd != "undefined" ? utmcmd : '';
    var cookie_utm_campaign = typeof utmccn != "undefined" ? utmccn : '';

    //Check if the URL has forced any property. If yes, then use it or use imoney.cookies.min.js as fallback
    var utm_source = getQueryVariable('utm_source') ? getQueryVariable('utm_source') : cookie_utm_src;
    var utm_medium = getQueryVariable('utm_medium') ? getQueryVariable('utm_medium') : cookie_utm_medium;
    var utm_term = getQueryVariable('utm_term') ? getQueryVariable('utm_term') : '';
    var utm_content = getQueryVariable('utm_content') ? getQueryVariable('utm_content') : '';
    var utm_campaign = getQueryVariable('utm_campaign') ? getQueryVariable('utm_campaign') : cookie_utm_campaign;
    var src_region = getQueryVariable('src') ? getQueryVariable('src') : '';

    return {
      'UTSOURCE': utm_source
      , 'UTMEDIUM': utm_medium
      , 'UTTERM': utm_term
      , 'UTCONTENT': utm_content
      , 'UTCAMPAIGN': utm_campaign
      , 'SRCREGION': src_region
    }
  }

  function get(propName) {
    return getAll()[propName];
  }
}();

var IMURLPropBag = function () {
  var sPageURL = window.location.search.substring(1);
  var sURLVariables = sPageURL.split('&');

  return {
    getAll: getAll
    , get: get
  };

  function getAll() {
    var userData = {};

    for (var i = 0; i < sURLVariables.length; i++) {
      if (sURLVariables[i] == '') continue;

      var sParameterName = sURLVariables[i].split('=');

      if (!userData[sParameterName[0]]) userData[sParameterName[0]] = [];
      userData[sParameterName[0]].push(sParameterName[1]);
    }

    _.each(userData, function (item, index) {
      userData[index] = userData[index].join(',');
      userData[index] = unescape(userData[index]);
      userData[index] = userData[index].replace(/\+/g, ' ');
    });

    return userData;
  }

  function get(propName) {
    return getAll()[propName];
  }
}();

var IMUTMZPropBag = function () {
  return {
    getAll: getAll
    , get: get
  };

  function getAll() {
    var utmz = {
      utmcsr: '',
      utmccn: '',
      utmcmd: '',
      utmctr: '',
      utmcct: ''
    };

    var gc = '';
    var c_name = "__utmz";

    if (document.cookie.length > 0) {
      c_start = document.cookie.indexOf(c_name + "=");
      if (c_start != -1) {
        c_start = c_start + c_name.length + 1;
        c_end = document.cookie.indexOf(";", c_start);
        if (c_end == -1) c_end = document.cookie.length;
        gc = unescape(document.cookie.substring(c_start, c_end));
      }
    }
    if (gc != "") {
      var y = gc.split('|');
      for (i = 0; i < y.length; i++) {
        if (y[i].indexOf('utmcsr=') >= 0) utmz.utmcsr = y[i].substring(y[i].indexOf('=') + 1);
        if (y[i].indexOf('utmccn=') >= 0) utmz.utmccn = y[i].substring(y[i].indexOf('=') + 1);
        if (y[i].indexOf('utmcmd=') >= 0) utmz.utmcmd = y[i].substring(y[i].indexOf('=') + 1);
        if (y[i].indexOf('utmctr=') >= 0) utmz.utmctr = y[i].substring(y[i].indexOf('=') + 1);
        if (y[i].indexOf('utmcct=') >= 0) utmz.utmcct = y[i].substring(y[i].indexOf('=') + 1);
      }
    }

    return utmz;
  }

  function get(propName) {
    return getAll()[propName];
  }
}();

var IMPropBag = function (bags) {
  return {
    addBag: addBag
    , getAllProps: getAllProps
  };

  function addBag(bag) {
    if (typeof bag !== 'object') return;
    bags.push(bag);
  }

  function getAllProps() {
    var props = {};
    _.each(bags, function (bag) {
      props = _.extend(props, bag.getAll());
    });

    return props;
  }
}([IMURLPropBag, IMiMoneyCookiesPropBag, IMUTMZPropBag, IMIMUPropBag, IMDevPropBag]);
//2nd Bag's property will replace the 1st Bag's properties if property key matches
//Always add IMDevPropBag at last. This allows devs to override any properties in any bags
/*****************************
 /* End of Property bags
 /*****************************/


/*********************************
 /* Start of Transport and Schema
 /*********************************/
var SubmissionTransport = function (jappConfig) {
  var that = this;
  this.jappConfig = jappConfig;

  this.send = function (params, callback, callback_params) {
    var result = {};
    var japp_vars = {merge_vars: {}};

    _.each(params, function (value, key) {
      japp_vars.merge_vars[key] = value;
    });

    jQuery.ajax({
      async: false,
      type: "POST",
      url: that.jappConfig.lcUrl,
      data: japp_vars,
      dataType: 'JSONP',
      jsonpCallback: 'callback',
      beforeSend: function () {
        if (typeof callback == 'function') callback(null, 'beforeSend', params, callback_params);
      },
      success: function (data) {
        result = data;
      },
      complete : function () {
        if (typeof callback == 'function') callback(result, 'complete', params, callback_params);
      }
    })
  }
};

var CampaignSchema = function (jappConfig) {
  var that = this;
  this.campaigns = [];
  this.jappConfig = jappConfig;

  this.get = function (callback) {
    if (!callback) {
      throw new Error('"Callback" is required!');
    }

    jQuery.ajax({
      async: true,
      type: "get",
      url: that.jappConfig.schemaUrl,
      dataType: 'JSONP',
      jsonpCallback: 'callback',
      beforeSend: function () {
      },
      success: function (campaignData) {
        that.campaigns = campaignData;

        //FIXME: id should be passed by the service.
        _.each(that.campaigns, function (campaign, index) {
          that.campaigns[index].id = that.campaigns[index]._id['$id'];
        });

        callback(that.campaigns);
      },
      error: console.error
    });
  }
};
/******************************
 /* End of Transport and Schema
 /******************************/


/********************************
 /* Start of Campaign management
 /********************************/
var Campaign = function (params, transport, propBag) {
  var that = this;
  _.each(params, function (a_param, key) {
    that[key] = a_param;
  });

  this.transport = transport;

  this.setTransport = function (transport) {
    this.transport = transport;
  };

  this.error = function (errMsg) {
    console.error(errMsg);
  };

  this.fieldExistsOrError = function (fieldName) {
    var fieldIndex = this.getFieldIndex(fieldName);
    if (fieldIndex === undefined) this.error('Field "' + fieldName + '" not found in Campaign ' + this.id);
    return fieldIndex;
  };

  this.setFieldValue = function (fieldName, value) {
    var fieldIndex = this.fieldExistsOrError(fieldName);
    if (fieldIndex === undefined) return;

    this.fields[fieldIndex].value = value;
  };

  this.getFieldValue = function (fieldName) {
    var field = this.getField(fieldName);
    if (field) return field.value;
  };

  this.getField = function (fieldName) {
    var fieldIndex = this.fieldExistsOrError(fieldName);
    if (fieldIndex === undefined) return;

    return _.clone(this.fields[fieldIndex]);
  };

  this.hasField = function (fieldName) {
    return _.findWhere(this.fields, {name: fieldName}) !== undefined;
  };

  this.getFieldIndex = function (fieldName) {
    var foundIndex = undefined;

    _.each(this.fields, function (a_field, index) {
      if (foundIndex) return;
      if (fieldName == a_field.name) foundIndex = index;
    });

    return foundIndex;
  };

  this.isValid = function () {
    var errorFields = _.filter(this.fields, function (a_field, index) {
      return a_field.required === true && typeof a_field.value === 'undefined';
    });

    if (errorFields.length > 0) {
      this.error('Missing value for the required fields!');
      this.error(errorFields);
      return false;
    }

    return true;
  };

  this.getParams = function () {
    var params = {
      campaign_id: this.id
    };

    _.each(this.fields, function (a_field) {
      params[a_field.name] = a_field.value;
    });

    if (typeof propBag !== 'undefined') {
      params = _.extend(propBag.getAllProps(), params);
    }

    return params;
  };

  this.submit = function (callback, callback_params) {
    if (!this.isValid()) return false;

    if (!this.transport) {
      this.error('Transport dependency is not met!');
      return;
    }

    var params = this.getParams();

    this.transport.send(params, callback, callback_params);
  };

  /*
   * Checks if propBag property matches with Campaign property.
   * If so then prefills the Campaign properties with the propBag properties.
   */
  this.propBagToFields = function () {
    if (typeof propBag === 'undefined') return;

    _.each(propBag.getAllProps(), function (prop, key) {
      if (that.hasField(key)) {
        that.setFieldValue(key, prop);
      }
    });
  }();
};

CampaignFactory = function (regionCode, env, readyCallback) {
  var that = this;
  this.regionCode = regionCode;
  this.env = env;
  this.campaigns = [];

  this.jappConfig = JAPPConfig.get(regionCode, env);
  if (!this.jappConfig) {
    throw new Error('JAPP config not found for regionCode: "' + regionCode + '" Env: "' + env + '"');
  }

  //Construct CampaignSchema
  this.campaignSchema = new CampaignSchema(this.jappConfig);
  this.campaignSchema.get(function (schema) {
    that.setSchema(schema);
    readyCallback(that);
  });

  //Construct Transport
  this.transport = new SubmissionTransport(this.jappConfig);

  this.setSchema = function (schema) {
    this.campaigns = schema;
  };

  this.getByNid = function (nId) {
    if (!nId) {
      throw new Error('nId is required');
      return;
    }

    var rawCampaign = _.findWhere(this.getRaw(), {nid: nId});

    if (!rawCampaign) {
      throw new Error('Campaign with nId ' + nId + ' is not found!');
    }

    return this.get(rawCampaign.id);
  };

  this.getByMachineName = function (machineName) {
    if (!machineName) {
      throw new Error('machineName is required');
      return;
    }

    var rawCampaign = _.findWhere(this.getRaw(), {machine_name: machineName});

    if (!rawCampaign) {
      throw new Error('Campaign with machineName ' + machineName + ' is not found!');
    }

    return this.get(rawCampaign.id);
  };

  this.get = function (campaignId) {
    if (!campaignId) {
      console.error('campaignId is required');
      return;
    }

    var rawCampaign = _.findWhere(this.getRaw(), {id: campaignId});

    if (!rawCampaign) {
      console.error('Campaign with id ' + campaignId + ' is not found!');
      return;
    }

    if (!rawCampaign.published == true) {
      console.error('Campaign with id ' + campaignId + ' is not published!');
      return;
    }

    var campaign_schema = {
      id: rawCampaign.id
      , label: rawCampaign.label
      , product_name: rawCampaign.product_name
      , product_type: rawCampaign.product_type
      , fields: []
    };

    _.each(rawCampaign.fields, function (a_field, key) {
      if (['submit'].indexOf(key) !== -1) return;

      campaign_schema.fields.push({
        name: a_field.name,
        type: a_field.type || a_field.extends,
        required: function () {
          if (typeof a_field.options === 'undefined') return false;
          if (typeof a_field.options.constraints === 'undefined') return false;
          if (typeof a_field.options.constraints.NotBlank === 'undefined') return false;

          return a_field.options.constraints.NotBlank.class === '.Symfony.Component.Validator.Constraints.NotBlank';
        }()
      })
    });

    return new Campaign(campaign_schema, this.transport, IMPropBag);
  };

  this.getRawOne = function (campaignId) {
    return _.findWhere(this.getRaw(), {id: campaignId});
  };

  this.getRaw = function () {
    return _.clone(this.campaigns);
  };
};


/*new CampaignFactory('ph', undefined, function(factory) {
 var cam = factory.get('55488e2d7fa493e8058b4568');

 cam.setFieldValue('name', 'Ahsanul');
 cam.setFieldValue('phone_number', '223344556677');
 cam.setFieldValue('email_address', 'foo@example.com');
 cam.setFieldValue('monthly_salary', 7000);
 cam.setFieldValue('have_another_credit_card', 'yes');

 cam.submit(function() {
 console.log(arguments);
 })
 });*/
/********************************
 /* End of Campaign management
 /********************************/

if (typeof exports !== 'undefined') {
  window.IMDevPropBag = IMDevPropBag
  window.CampaignFactory = CampaignFactory;
  window.Campaign = Campaign;
  window.SubmissionTransport = SubmissionTransport;
  window.CampaignSchema = CampaignSchema;
}
