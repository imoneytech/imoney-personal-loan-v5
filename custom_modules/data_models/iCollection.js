/**
 * Feature of module and how to use it.
 *
 * 1. Requirements:
 * These modules are mandatory:
 * - backbone
 * - winston
 * - node-fetch
 *
 * These variables are mandatory in global level:
 * _ global.config.app or global.config
 *
 * 2. Variables and return value:
 * @string modelName: Represent the name of the model, for example 'credit-card'
 * @string url: Data source for model.
 * @string|@function value:
 *    Extract value from an attribute of data source if is string.
 *    Using the whole json from data source if is not define.
 *    Provide a callback to manually set data for collection.
 *
 * @return BackboneCollection with some extended methods.
 *
 * 3. Public extended methods of return values:
 * update: Send a request of updating data so that Model will see if data is
 *         stale. If not, data remains the same. If it is, call a fetch.
 * fetch: Forcing Model to get new data regardless how old the data is. If
 *        requesting data from server fail, old set of data remains the same.
 *
 * 4. Example of using the module.
 * const CreditCard = new iCollection(
 *    'credit card',
 *    'https://direct.imoney.my/profile/getCreditCard/my/en',
 *    'cards',
 *    30000
 * );
 *
 */


'use strict';

// Modules.
const Backbone = require('backbone'),
    winston = require('winston'),
    fetch = require('node-fetch');

// Default variables.
const nodeEnv = global.nodeEnv || process.env.NODE_ENV,
    port = global.port || process.env.PORT,
    config = global.config.app || global.config || {},
    liveTime = config.TTL || 30000,
    debugName = global.debugName || 'Application';

// Public module.
const iCollection = function(modelName = 'product', url, value = undefined,
                             attribute = 'path_alias', TTL = liveTime) {
// Debug.
  const debug = require('debug')(
      `${debugName}:customModules:iCollection:${modelName}`),
      error = require('debug')(
          `${debugName}:customModules:iCollection:${modelName}:error`);

  if (!url) {
    throw new ReferenceError('No fetch URL defined');
  }
  let Model = Backbone.Model.extend({idAttribute: attribute});
  const purge = async function(callback) {
    // Avoiding multiple request in data expired time.
    if (this.lock) return;

    if (process.env.DATA === 'local') {
      // https://direct.imoney.my/profile/getBank/my/en
      this.url = this.url.replace(/https?:\/\/[^/]+(\/.+)$/g,
          `http://localhost:${port}$1`);
    }

    debug(`[${new Date()}]: Sending requests to ${this.url}`);
    this.lock = true;

    var dataString;
    try {
      dataString = await fetch(this.url);
    }
    catch (e) {
      this.lock = false;
      if (typeof callback === 'function') callback(e);
      console.log(e);
      throw e;
    }
    let jsonData = await dataString.json();
    // If not defined, set the whole JSON.
    if (value === undefined) {
      this.set(jsonData);
    }
    // If defined as a string, get that property from JSON.
    else if (typeof value === 'string') {
      this.set(jsonData[value]);
    }
    // Or use a custom set by function.
    else if (typeof value === 'function') value.call(this, jsonData);

    this.updated = Date.now();
    debug(`${this.length} ${this.name}(s) loaded.`);

    if (typeof callback === 'function') callback();
    this.lock = false;

    dataString = null;
    jsonData = null;
  };

  this.productCollection = new (Backbone.Collection.extend({
    name: modelName,
    model: Model,
    url: url,
    TTL: TTL,
    updated: 0,
    update(callback) {
      if (Date.now() - this.updated > this.TTL) {
        this.fetch(callback);
      }
    },
    initialize() {
      this.update();
    },
    fetch(callback) {
      purge.call(this, function() {
        if (typeof callback === 'function') callback();
      }).catch(e => {
        error(`Request to ${this.url} failed.`);
        error(`Current stale data has ${this.length} ${this.name}(s),
      expired at ${new Date(this.updated + this.TTL)}`);
        error(e);
      });
    },
  }));

  this.addMethods = function(methods) {
    Object.entries(methods).forEach(([name, func]) => {
      this.productCollection[name] = func;
    });
  };
};

exports = module.exports = iCollection;
