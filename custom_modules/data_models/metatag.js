'use strict';
const iCollection = require('./iCollection');

input => ({
  pageTitle: input.seo[0].meta_title,
  ogTitle: input.seo[0].og_title || input.seo[0].meta_title,
  metaDescription: input.seo[0].meta_description,
  ogDescription: input.seo[0].og_description || input.seo[0].meta_description,
  metaKeyword: '',
  canonicalURL: input.seo[0].cannonical_url ||
      `${config['liveUrl']}/${input.path}`,
  ogImage: '',
  siteName: config['site_name'],
  ogType: 'website',
});

const metaCollection = new iCollection('metatag', config.app.dataUrls.metatag,
    undefined, 'path');

metaCollection.addMethods({
  query(url) {
    return this.get(url);
  },
});

exports = module.exports = metaCollection.productCollection;