'use strict';
// const iCollection = require('../iCollection');
// const plCollection = new iCollection('personal-loan',
//   global.config.app['dataUrls'].personalLoan, undefined, 'nid');
// exports = module.exports = plCollection.productCollection;



const personalLoan = global.personalLoan = require("../iCollection")(global.config.app['dataUrls'].personalLoan);

var Q = require('q');
var productPromise = [];
productPromise = Q.nbind(personalLoan.fetch, personalLoan);
productPromise().then(function() {
    console.log(personalLoan.models.length + ' Listing pages!');
}, function(err) {
    console.log('Products could not be loaded: ' + err.toString());
    process.exit();
});

exports = module.exports = personalLoan;