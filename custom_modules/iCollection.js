'use strict';

/**
 * Module dependencies.
 * @private
 */
const
    Backbone = require('backbone'),
    request = require('request');

let Product = Backbone.Model.extend({
    idAttribute: "nid",
    url: function () {
        return this;
    }
});

/**
 * Module exports.
 * @public
 */
exports = module.exports = function (url) {
    return new (Backbone.Collection.extend({
        model: Product,
        fetch: function () {
            request(url, (error, response, body) => {
                if (!error && response.statusCode == 200) {
                    this.set(JSON.parse(body));
                }
            })
            .on('error', function(err) {
                console.log(err)
            });
        }
    }));
};