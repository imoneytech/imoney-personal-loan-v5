const winston = require('winston');
winston.level = global.config.logLevel || 'info';


/**
 * Specify where the logs should be pushed to.
 * Can be pushed to any streams.
 * Find out more: https://github.com/winstonjs/winston
 */
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({ colorize: true }),
    //new (winston.transports.File)({ filename: 'imoney-api.log' })
  ]
});

module.exports = logger;