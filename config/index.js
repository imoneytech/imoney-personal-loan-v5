const debug = require('debug')('config');
const config = require('./app.json');

const region = process.env.REGION;
const lang = process.env.LANG;

if (!process.env.NODE_ENV) throw 'NODE_ENV NOT SET!';
if (!lang) throw 'LANG NOT SET!';
if (!region) throw 'No REGION set!';
if (!config[`${region}-${lang}`]) throw `Region '${region}' has no config for ${lang}!`;

debug(`Current REGION is set to ${region}`);

const appConfig = Object.assign(config.default, config[`${region}-${lang}`],
    {region, lang});
debug('Loading APP with following config:', appConfig);

module.exports = {
  app: appConfig,
};
