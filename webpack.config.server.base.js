const webpack = require("webpack");
const path = require("path");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const nodeExternals = require("webpack-node-externals");
const config = require("./config");

let assetsPlaceHolder = config.app.assetsPlaceHolder;

function resolve(dir) {
 return path.join(__dirname, "..", dir);
}

console.log(path.join(__dirname, `/src/scss/${config.app.region}/filter.scss`));
const webpackConfig = {
 context: path.resolve(__dirname),
 target: "node",
 output: {
  path: path.resolve(__dirname, "dist"),
  filename: "build.js",
  libraryTarget: "commonjs2"
 },
 module: {
  rules: [
   {
    test: /\.js$/,
    loader: "babel-loader",
    exclude: /node_modules/,
    include: [resolve("src")]
   },
   {
    test: /\.vue$/,
    loader: "vue-loader",
    options: {
     cacheBusting: true,
     cssSourceMap: true,
     transformToRequire: {
      video: ["src", "poster"],
      source: "src",
      img: "src",
      image: "xlink:href"
     }
    }
   },
   {
    test: /\.scss$/,
    use: ["vue-style-loader", "css-loader", "sass-loader"]
   },
   {
    test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
    use: [
     {
      loader: "file-loader",
      options: {
       name: `${assetsPlaceHolder}/font/[name].[ext]`
      }
     }
    ]
   },
   {
    test: /\.(png|jpg|gif|svg)$/,
    use: [
     {
      loader: "file-loader",
      options: {
       name: `/${assetsPlaceHolder}/img/[name].[ext]`
      }
     }
    ]
   }
  ]
 },
 resolve: {
  extensions: ["*", ".js", ".vue"],
  alias: {
   "faq.vue": path.join(
    __dirname,
    `/src/app/components/blocks/faq/${config.app.region}-${
     config.app.lang
    }-faq.vue`
   ),
   "faq-online-loan.vue": path.join(
    __dirname,
    `/src/app/components/blocks/faq/${config.app.region}-${
     config.app.lang
    }-faq-online-loan.vue`
   ),
   'faq-business-loan.vue' : path.join(
    __dirname,
    `/src/app/components/blocks/faq/${config.app.region}-${
     config.app.lang
    }-faq-business-loan.vue`
   ),
   "./img": path.join(__dirname, "src/img"),
   "applyForm.vue": path.join(
    __dirname,
    `/src/app/components/modals/applyForms/${config.app.region}/applyForm.vue`
   ),
   "bus.js": path.join(__dirname, "src/app/bus.js"),
   vue$: "vue/dist/vue.esm.js",
   "./font": path.join(__dirname, "src/imoney-assets/font"),
   "@": resolve("src"),
   'helper.js': path.join(__dirname, `src/app/lib/${config.app.region}/helper.js`),
   "pdfobject.js": path.join(__dirname, "src/app/lib/pdfobject.js"),
   calculation: path.join(
    __dirname,
    `/src/app/lib/${config.app.region}/calculation.js`
   ),
   sideFilter: path.join(
    __dirname,
    `/src/app/components/${config.app.region}/sideFilter.vue`
   ),
   filter: path.join(__dirname, `/src/app/lib/${config.app.region}/filter.js`),
   store: path.join(__dirname, `/src/app/store/${config.app.region}/index.js`),
   'numberFormat': path.join(__dirname, `/src/app/lib/${config.app.region}/numberFormat.js`),
   calculator: path.join(
    __dirname,
    `/src/app/components/${config.app.region}/calculator.vue`
   ),
   "pageMenu.vue": path.join(
    __dirname,
    `/src/app/components/${config.app.region}/pageMenu.vue`
   ),
   './product.vue': path.join(
    __dirname,
    `/src/app/pages/${config.app.region}/product.vue`,
  ),
  './listing.vue': path.join(
    __dirname,
    `/src/app/pages/${config.app.region}/listing.vue`,
  ),
  'filterMobile.vue' : path.join(__dirname, `/src/app/components/modals/${config.app.region}/filterMobile.vue`),
  "./filter.scss": path.join(
    __dirname,
    `/src/scss/${config.app.region}/filter.scss`
   ),
   './list-item.scss' : path.join(__dirname, `/src/scss/${config.app.region}/list-item.scss`),
  }
 },
 externals: nodeExternals({
  whitelist: /\.css$/
 })
};

module.exports = webpackConfig;
