const express = require("express");

const router = express.Router();
const axios = require("axios");

const path = require("path");
const { createBundleRenderer } = require("vue-server-renderer");

const toggleConfig = require('../src/config');
const featureToggleList = toggleConfig.featureToggle.featureToggleList;
// const rendererProduct = createBundleRenderer(
//     path.join(__dirname, '../dist/vue-ssr-server-product-bundle.json'),
// );

const rendererList = createBundleRenderer(
 path.join(__dirname, "../dist/vue-ssr-server-list-bundle.json")
);

const rendererProduct = createBundleRenderer(
 path.join(__dirname, "../dist/vue-ssr-server-product-bundle.json")
);

const rendererTQBUI = createBundleRenderer(
 path.join(__dirname, "../dist/vue-ssr-server-tqbui-bundle.json")
);

const PersonalLoan = global.PersonalLoan;
const Metadata = global.metadata;
const config = global.config.app;
console.log();
function featuredProducts(personalLoan,type) {
 return config.region ==='ph' ? personalLoan.filter(key => key.promo === true && key.loan_type === type):
 personalLoan.filter(key => key.promo === true);
}

router.get("/", function(req, res) {
 res.send("GET request to the homepage");
});

/**
 * Listing Page.
 */
config.urls.forEach(function(link) {
 router.get(link.url, (req, res) => {
  if (req.url.endsWith("/")) {
   res.redirect(301, req.url.replace(/\/$/, ""));
  }
  const context = {
   url: req.url,
   tr: res.__,
   PersonalLoan: link.djangoIndexApi.filter
    ? PersonalLoan.toJSON().filter(key => key[link.djangoIndexApi.key] === link.djangoIndexApi.value)
    : PersonalLoan.toJSON(),
   config: {
    lang: config["lang"],
    domain: config["domain"],
    region: config["region"],
    urls: config["urls"],
    dataUrls: config["dataUrls"],
    industries: config["industries"],
    tenureList: link.tenureList,
    monthly: link.monthly,
    currency: config.currency,
    url: link.url,
    features: config.features,
    defaultValues: link.defaultValues,
    featureToggleList
   },
   pageType: link.type
  };
  let base = {};
  rendererList
   .renderToString(context)
   .then(html => {
    let metatag;
    try {
     const metadata = Metadata.get(
      link.metaDataUrl.replace("online-loan-b", "online-loan")
     );
     metatag = metadata.get("seo")[0];
     metatag.fbAppId = config.fbAppId;
    } catch (e) {
     metatag = {};
    }
    res.render("../dist/listing.html", {
     pageTitle: metatag.meta_title || "iMoney",
     bodyClass: `imoney-${config.region}`,
     metaData: metatag,
     meta: metatag,
     itemData: metatag,
     ogImage: metatag.ogImage,
     metaDescription: metatag.meta_description,
     ogDescription: metatag.og_description,
     metaKeyword: metatag.metaKeyword,
     pageHeader: "Page Header",
     pageDescription: "Page Description",
     region: config.region,
     lang: config.lang,
     app: config,
     path_alias: metatag.path,
     metatag,
     assetsPlaceHolder: config.assetsPlaceHolder,
     config: { ...config, featureToggleList },
     base,
     data: context,
     ComponentMarkup: html
    });
   })
   .catch(error => {
    console.log(error);
   });
 });
});

personalLoan.on("add", function(product) {
 console.log(`/${product.attributes.path_alias}`);
 router.get(config.abTestingEnable ? 
  `/${product.attributes.path_alias.replace(
   "personal-loan",
   "personal-loan-b"
  )}` : `/${product.attributes.path_alias}`,
  (req, res, next) => {
   let nid = product.get("nid");
   let loan = personalLoan.get(nid).toJSON();
   if (req.url.endsWith("/")) {
    res.redirect(301, req.url.replace(/\/$/, ""));
   }
   const context = {
    url: '/personal-loan',
    applyParmValue: req.query.apply,
    config: {
     lang: config["lang"],
     domain: config["domain"],
     region: config["region"],
     urls: {
      listing : '/personal-loan',
     },
     industries: config["industries"],
     currency: config.currency,
     monthly: ["online-loan","business-loan"].indexOf(loan.loan_type) >= 0,
     featureToggleList,
     tenureList: function() {
      let array = [];
      let start = parseFloat(loan.min_tenure);
      let end = parseFloat(loan.max_tenure);
      if (["online-loan","business-loan"].indexOf(loan.loan_type) >= 0) {
       for (var i = start; i <= end; i++) {
        if([7,8,10,11].indexOf(i) < 0 ){
          array.push(i);
        }
       }
      } else {
       for (var i = start; i <= end; i++) {
        array.push(i);
        if (i + 0.5 <= end) {
            array.push(i + 0.5);
        }
       }
      }
      // console.log(array);
      return array;
     }(),
    },
    tr: req.__,
    loan: loan,
    featuredProducts: featuredProducts(PersonalLoan.toJSON(),loan.loan_type)
   };
   const base = {};
   let metatag;
   try {
    metatag = loan.seo[0];
    metatag.fbAppId = config.fbAppId;
   } catch (e) {
    metatag = {};
   }
   rendererProduct
    .renderToString(context)
    .then(html => {
     res.render("../dist/product.html", {
      pageTitle: metatag.meta_title || "iMoney",
      bodyClass: `imoney-${config.region}`,
      metaData: metatag,
      meta: metatag,
      itemData: metatag,
      ogImage: metatag.ogImage,
      metaDescription: metatag.meta_description,
      ogDescription: metatag.og_description,
      metaKeyword: metatag.metaKeyword,
      pageHeader: "",
      pageDescription: "",
      region: config.region,
      lang: config.lang,
      app: config,
      path_alias: metatag.path,
      metatag,
      assetsPlaceHolder: config.assetsPlaceHolder,
      config: {...config, featureToggleList },
      base,
      data: context,
      ComponentMarkup: html
     });
    })
    .catch(error => {
     console.log(error);
    });
  }
 );
});

config.thankYouurls.forEach(function(link) {
  console.log(link);
  router.get(link, (req, res) => {
    const base = {};
    let metatag = {};
    res.render("../dist/thankyou.html", {
     pageTitle: metatag.meta_title || "iMoney",
     bodyClass: `imoney-${global.config.app.region}`,
     region: config.region,
     lang: config.lang,
     app: config,
     path_alias: metatag.path,
     metaDescription: metatag.meta_description,
     metaKeyword: metatag.metaKeyword,
     metaData: metatag,
     meta: metatag,
     itemData: metatag,
     ogImage: metatag.ogImage,
     ogDescription: metatag.og_description,
     metatag,
     data: config,
     assetsPlaceHolder: config.assetsPlaceHolder,
     config: {...config, featureToggleList },
     base
    });
   });
});



/**
 * For bypassing images.
 */
router.get("/uploads/*", (req, res, next) => {
 try {
  res.redirect(`${config["dataUrls"].images}${req.path}`);
 } catch (e) {
  next();
 }
});

module.exports = router;
