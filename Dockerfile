#-- Build
FROM node:8-alpine as builder
LABEL maintainer Ross Affandy <ross.affandy@imoney-group.com>

ARG ssh_private_key
ARG build_region
ARG env
ARG lang
ARG ENV
ARG MARKET
ENV build_region=${build_region}
ENV env=${env}
ENV lang=${lang}
ENV ENV=${ENV}
ENV MARKET=${MARKET}

RUN mkdir /root/.ssh/ && \
    chmod 0700 /root/.ssh && \
    apk add --no-cache --virtual .builddeps openssh-client git build-base make gcc g++ python && \
    ssh-keyscan bitbucket.org > /root/.ssh/known_hosts && \
    echo "${ssh_private_key}" > /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa

WORKDIR /app

COPY . /app

RUN cp /app/config/app.json.sample /app/config/app.json

RUN NR_NATIVE_METRICS_NO_BUILD=true npm install --unsafe-perm && \
    npm run build:${build_region} && \
    rm /root/.ssh/id_rsa && \
    apk del .builddeps


#-- Release
FROM node:8-alpine

ARG ssh_private_key
ARG build_region
ARG ENV
ARG MARKET
ENV build_region=${build_region}
ENV ENV=${ENV}
ENV MARKET=${MARKET}

RUN mkdir /root/.ssh/ && \
    chmod 0700 /root/.ssh && \
    apk add --no-cache --virtual .builddeps openssh-client git build-base make gcc g++ python && \
    ssh-keyscan bitbucket.org > /root/.ssh/known_hosts && \
    echo "${ssh_private_key}" > /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa

WORKDIR /app

COPY . /app

RUN cp /app/config/app.json.sample /app/config/app.json

RUN NR_NATIVE_METRICS_NO_BUILD=true npm install --unsafe-perm --production && \
    npm cache clean --force && \
    rm /root/.ssh/id_rsa && \
    apk del .builddeps

COPY --from=builder /app/dist ./dist

EXPOSE 3000

ENTRYPOINT ["/bin/sh"]

CMD ["-c" , "npm run start:${build_region}"]
